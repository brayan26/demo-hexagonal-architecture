package com.example.demo.application.uses_cases.customer.create;

import com.example.demo.domain.customer.Customer;
import com.example.demo.infrastructure.services.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreateCustomer {
    @Autowired
    CustomerService customerService;

    public Customer create(Customer dto) {
        return customerService.save(dto);
    }
}
