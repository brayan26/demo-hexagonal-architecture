package com.example.demo.application.uses_cases.customer.search;

import com.example.demo.domain.customer.Customer;
import com.example.demo.infrastructure.services.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FindAllCustomer {
    @Autowired
    CustomerService customerService;

    public List<Customer> find() {
        return customerService.findAll();
    }
}
