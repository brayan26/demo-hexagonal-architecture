package com.example.demo.application.uses_cases.customer.search;

import com.example.demo.domain.customer.Customer;
import com.example.demo.infrastructure.services.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FindCustomerById {
    @Autowired
    CustomerService customerService;

    public Customer find(String id){
        return customerService.findById(id);
    }
}
