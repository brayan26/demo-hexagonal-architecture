package com.example.demo.application.uses_cases.customer.update;

import com.example.demo.domain.customer.Customer;
import com.example.demo.infrastructure.services.customer.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UpdateCustomer {
    @Autowired
    CustomerService customerService;

    public Customer update(Customer dto) {
        return customerService.update(dto);
    }
}
