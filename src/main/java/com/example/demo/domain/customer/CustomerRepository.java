package com.example.demo.domain.customer;

import java.util.List;

public interface CustomerRepository {
    /**
     *
     * @param dto to save
     * @return Customer registered
     */
    Customer save(Customer dto);

    /**
     *
     * @param dto to update
     * @return Customer updated
     */
    Customer update(Customer dto);

    /**
     *
     * @param id to filter
     * @return Customer registered
     */
    Customer findById(String id);

    /**
     *
     * @return A Customer list
     */
    List<Customer> findAll();


}
