package com.example.demo.domain.invoce_header;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class InvoiceHeader {
    private String sucursal;
    private String concepto;
    private Long documento;
    private String fecha;
    private String idTercero;
    private String nombreTercero;
    private String apellidoTercero;
    private Boolean estado;
    //private List<InvoiceBody> items;
    private Double subtotal;
    
}
