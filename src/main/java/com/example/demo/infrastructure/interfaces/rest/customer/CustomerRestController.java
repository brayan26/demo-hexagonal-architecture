package com.example.demo.infrastructure.interfaces.rest.customer;

import com.example.demo.application.uses_cases.customer.create.CreateCustomer;
import com.example.demo.application.uses_cases.customer.search.FindAllCustomer;
import com.example.demo.application.uses_cases.customer.search.FindCustomerById;
import com.example.demo.application.uses_cases.customer.update.UpdateCustomer;
import com.example.demo.domain.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/customer")
public class CustomerRestController implements ICustomerRestController {
    @Autowired
    CreateCustomer createCustomer;

    @Autowired
    UpdateCustomer updateCustomer;

    @Autowired
    FindAllCustomer findAllCustomer;

    @Autowired
    FindCustomerById findCustomerById;

    @Override
    public ResponseEntity<?> save(@RequestBody Customer dto) {
        try {
            return new ResponseEntity<>(createCustomer.create(dto), HttpStatus.CREATED);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @Override
    public ResponseEntity<?> update(@RequestBody Customer dto) {
        try {
            return new ResponseEntity<>(updateCustomer.update(dto), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<>(findCustomerById.find(id), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    public ResponseEntity<?> findAll() {
        try {
            return new ResponseEntity<>(findAllCustomer.find(), HttpStatus.OK);
        }catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
