package com.example.demo.infrastructure.interfaces.rest.customer;

import com.example.demo.domain.customer.Customer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

public interface ICustomerRestController {
    @PostMapping(value = "/save", produces = {"application/json"})
    ResponseEntity<?> save(@RequestBody Customer dto);

    @PatchMapping(value = "/update", produces = {"application/json"})
    ResponseEntity<?> update(@RequestBody Customer dto);

    @GetMapping(value = "/findById/{id}", produces = {"application/json"})
    ResponseEntity<?> findById(@PathVariable String id);

    @GetMapping(value = "/findAll", produces = {"application/json"})
    ResponseEntity<?> findAll();
}
