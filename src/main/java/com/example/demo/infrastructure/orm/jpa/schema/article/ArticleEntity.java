package com.example.demo.infrastructure.orm.jpa.schema.article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "article")
public class ArticleEntity {
    @Id
    private String codigo;
    private String descripcion;
    private Double precioUnitario;
}
