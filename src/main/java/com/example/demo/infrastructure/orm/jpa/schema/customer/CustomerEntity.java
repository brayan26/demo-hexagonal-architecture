package com.example.demo.infrastructure.orm.jpa.schema.customer;

import com.example.demo.domain.customer.Customer;
import com.example.demo.infrastructure.orm.jpa.schema.invoice_header.InvoiceHeaderEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "customer")
public class CustomerEntity implements Serializable {
    @Id
    @Column(length = 15)
    private String id;
    private String name;
    @Column(name = "last_name")
    private String lastName;
    private Integer age;
    private String genero;
    @OneToMany(mappedBy = "tercero")
    private List<InvoiceHeaderEntity> factura;
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreateAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateAt;

    public Customer serialize() {
        return new ModelMapper().map(this, Customer.class);
    }

    public static CustomerEntity createEntityFromDto(Customer dto) {
        return new ModelMapper().map(dto, CustomerEntity.class);
    }
}
