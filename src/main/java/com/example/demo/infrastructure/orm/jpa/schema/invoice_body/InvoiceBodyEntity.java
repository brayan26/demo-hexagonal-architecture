package com.example.demo.infrastructure.orm.jpa.schema.invoice_body;

import com.example.demo.infrastructure.orm.jpa.schema.article.ArticleEntity;
import com.example.demo.infrastructure.orm.jpa.schema.customer.CustomerEntity;
import com.example.demo.infrastructure.orm.jpa.schema.invoice_header.InvoiceHeaderEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "invoice_body")
public class InvoiceBodyEntity implements Serializable {
    @EmbeddedId
    private InvoiceBodyEntityPK pk;
    private Double cantidad;
    private Double precioVenta;
    @ManyToOne
    @JoinColumn(name = "codigo_articulo_fk", referencedColumnName = "codigo")
    private ArticleEntity articulo;
    @MapsId(value = "idDocumento")
    @ManyToOne
    @JoinColumn(name = "invoice_id_fk", referencedColumnName = "idDocumento")
    private InvoiceHeaderEntity tercero;
}
