package com.example.demo.infrastructure.orm.jpa.schema.invoice_body;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class InvoiceBodyEntityPK implements Serializable {
    private Integer item;
    private Long idDocumento;
}
