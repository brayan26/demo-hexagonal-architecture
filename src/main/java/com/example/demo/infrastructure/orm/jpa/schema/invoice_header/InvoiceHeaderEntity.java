package com.example.demo.infrastructure.orm.jpa.schema.invoice_header;

import com.example.demo.infrastructure.orm.jpa.schema.customer.CustomerEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "invoice_header")
public class InvoiceHeaderEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idDocumento;
    private String sucursal;
    private String concepto;
    private Long numeroDocumento;
    private String fecha;
    @ManyToOne
    @JoinColumn(name = "tercero_id_fk", referencedColumnName = "id")
    private CustomerEntity tercero;
    private Boolean estado;
    @Temporal(TemporalType.TIMESTAMP)
    private Date CreateAt;
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateAt;
}
