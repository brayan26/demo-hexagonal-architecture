package com.example.demo.infrastructure.orm.jpa.schema.roles;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "security_role")
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createAt;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date updateAt;
}
