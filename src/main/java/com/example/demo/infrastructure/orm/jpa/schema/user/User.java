package com.example.demo.infrastructure.orm.jpa.schema.user;

import com.example.demo.infrastructure.orm.jpa.schema.roles.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "security_user")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String lastName;
    @Column(unique = true)
    private String email;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date createAt;
    @Temporal(value = TemporalType.TIMESTAMP)
    private Date updateAt;
    @ManyToMany
    private List<Role> roles;
}
