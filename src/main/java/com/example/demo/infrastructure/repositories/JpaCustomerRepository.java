package com.example.demo.infrastructure.repositories;

import com.example.demo.infrastructure.orm.jpa.schema.customer.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaCustomerRepository extends JpaRepository<CustomerEntity, String> {
}
