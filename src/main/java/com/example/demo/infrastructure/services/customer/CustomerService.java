package com.example.demo.infrastructure.services.customer;

import com.example.demo.domain.customer.Customer;
import com.example.demo.domain.customer.CustomerRepository;
import com.example.demo.infrastructure.orm.jpa.schema.customer.CustomerEntity;
import com.example.demo.infrastructure.repositories.JpaCustomerRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CustomerService implements CustomerRepository {
    @Autowired
    JpaCustomerRepository customerRepository;

    @Override
    public Customer save(Customer dto) {
        log.info("Saving customer");
        return customerRepository.save(
                CustomerEntity.createEntityFromDto(dto)
        ).serialize();

    }

    @Override
    public Customer update(Customer dto) {
        log.info("Updating customer");
        return customerRepository.save(
                CustomerEntity.createEntityFromDto(dto)
        ).serialize();
    }

    @Override
    public Customer findById(String id) {
       Optional<CustomerEntity> entity = customerRepository.findById(id);

       if (!entity.isPresent()) {
           throw new RuntimeException("Customer id: "+ id +" Not found");
       }

       return entity.get().serialize();
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll()
                .stream()
                .map(CustomerEntity::serialize)
                .collect(Collectors.toList());
    }
}
